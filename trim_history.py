#!/usr/bin/env python3
import re

#TODO: input output params

# code --list-extensions | xargs -L 1 echo code --install-extension
# curl -s -XPOST -d '{"auth":{"RAX-KSKEY:apiKeyCredentials":{"username":"mimic","apiKey":"12345"}}}' http://localhost:8900/identity/v2.0/tokens | jq -r\
# sysctl -w fs.file-max=65536
# sysctl -w vm.max_map_count=262144 sysctl -w fs.file-max=65536 ulimit -n 65536 ulimit -u 4096
# sudo sysctl -w vm.max_map_count=262144 sudo sysctl -w fs.file-max=65536 ulimit -n 3072 ulimit -u 4096
# ulimit -n 307
# sudo chgrp docker /lib/systemd/system/docker.socket
# sudo chgrp $USER /lib/systemd/system/docker.socket
# sudo chmod g+w /lib/systemd/system/docker.socket
# sudo journalctl --vacuum-time=3d

# TODO: save cmd if second word not in cmd list or symbol

start = ('git', 'DOCKER', 'docker', 'curl', 'wget', 'find', 'kubectl', 'helm', 'apt', 'tar', 'x11docker', 'minikube', 'fastboot')
contain = ('awk', '>', '|')

keep = []

with open('origin.txt', 'r') as reader:
    txt = reader.read()
    cmds = txt.split(re.findall(': \d{10}:\d;', txt)[0])
    for i in cmds:
        if i.startswith(start):
            keep.append(i)
        # for asdf in contain:
        #     if asdf in i:
        #         keep.append(i)

keep = set(keep)


# for command in keep:
#     print('command: ', command)

with open('trimmed.txt', 'w') as f:
    for command in keep:
        f.write("%s" % command)
