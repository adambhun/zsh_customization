# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/morfeusz/.oh-my-zsh"

ZSH_THEME="powerlevel10k/powerlevel10k"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
ZSH_COLORIZE_TOOL=pygmentize
ZSH_COLORIZE_STYLE="tango"
plugins=(colored-man-pages colorize compleat command-not-found dircycle docker docker-compose dotenv git github helm kops kubectl minikube terraform vscode zsh-autosuggestions zsh-syntax-highlighting zsh-history-substring-search zsh-completions history-sync)
# may be needed someday: aws, golang, go

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nedit'
else
  export EDITOR='vim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

if [ $commands[kubectl] ]; then source <(kubectl completion zsh); fi

eval $(thefuck --alias)

export CHANGE_MINIKUBE_NONE_USER=true

#COMPLETION
###########################################################################################

autoload -Uz compinit bashcompinit
compinit
bashcompinit

source ~/.bash_completion.d/compleat_setup

zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

source /home/morfeusz/zaw/zaw.zsh
eval bindkey '^r' zaw-history

###########################################################################################


#HISTORY-STUFF
###########################################################################################

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS=""

HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000000
SAVEHIST=10000000

setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
setopt HIST_IGNORE_SPACE         # Do not write command to history if it starts with a whitespace

declare -a always_ignore=(cd ls la ll man rm)
declare -A lvl_1_ignore
declare -a git=(add commit inspect)
declare -a docker=(add commit inspect history)
lvl_1_ignore[git]=${git[@]}
lvl_1_ignore[docker]=${docker[@]}

l1k=${(k)lvl_1_ignore}
IFS=' ' read -r -A lvl1keys <<< "$l1k"
# # called before a history line is saved.  See zshmisc(1).
# # Note: always saves the penultimate command not the last.
function zshaddhistory() {
  #####################################################################################################################
  # How this works:
  # This function works in conjunction with the "precmd" function (defined below this one), and prevents commands
  # from being written to history before they are executed; instead, they will be saved to "LASTHIST", which
  # is temporary, so you can recall failed commands too, as long as you remain in the same session (leave
  # the terminal window open.)
  #
  # Commands starting with the words in "always_ignore" will not be written to .zsh_history
  # "lvl_1_ignore" is an associative array, so much like a dictionary in other languages. Its values are lists.
  # If the first word of a command is found among the keys of 'lvl_1_ignore' the script searches for the command's
  # second word in the values at 'lvl_1_ignore'[<first word>]. If the search is successful, the command will not
  # be stored in .zsh_history
  #####################################################################################################################
  emulate -L zsh

  local -a command
  command=(${(Q)${(z)1}})

  #! zsh arrays start with 1, not 0
  # Write the command to history in precmd if it meets the following conditions.
  if [[ ${always_ignore[(i)$command[1]]} -le ${#always_ignore} ]] ; then
    return 1
  else
    if [[ ${lvl1keys[(i)$command[1]]} -le ${#lvl_1_ignore} ]] ; then
      subignore=${(P)command[1]}
      if [[ ${subignore[(i)$command[2]]} -le ${#subignore} ]] ; then
        return 1
      fi
    fi
  fi
  # Return value 2: "... the history command will be saved on the internal - temporary - history list,
  # but not written to the history file".
  LASTHIST=$1
  return 2
}

# zsh hook called before the prompt is printed.  See zshmisc(1).
function precmd() {
  # Write the last command if successful, using the history buffered by
  # zshaddhistory().
  if [[ $? == 0 && -n $LASTHIST && -n $HISTFILE ]] ; then
    print -sr -- ${LASTHIST%%'\n'}
  fi
}

# HISTORY SYNC
#######################################################################################################################

ZSH_HISTORY_FILE="${HOME}/.zsh_history"
ZSH_HISTORY_PROJ="${HOME}/.zsh_history_proj"
ZSH_HISTORY_FILE_ENC="${ZSH_HISTORY_PROJ}/zsh_history"
GIT_COMMIT_MSG="latest $(date)"
#######################################################################################################################
